#!/usr/bin/env python3

import yaml
import os
#import sys

#sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))



file_path = os.path.dirname(os.path.realpath(__file__))

def get_inventory():
    with open(file_path + "/inventory.yml", "r") as i:
        inventory_list = yaml.safe_load(i.read())

    return inventory_list


if __name__ == "__main__":
    get_inventory()