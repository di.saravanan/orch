#!/usr/local/bin/python3

from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import ConfigLoadError, CommitError, ConnectError, LockError, UnlockError
from lxml import etree
from jinja2 import Template
from loguru import logger
import json
import yaml
import time

logger.add('log/junos_api.log', format="{time} {level} | {module}:{function}| {message}", level="DEBUG")

class junos(object):
    def __init__(self, host, username, password):
        
        self.host = host
        self.username = username
        self.password = password
        self.dev = Device(host=self.host, user=self.username, passwd=self.password)
        self.conn_status = self.connect()
        self.config_locked = False


    @logger.catch
    def connect(self):
        """[Connect to device]

        Returns:
            [Boolean]: [Connection status to device]
        """
        try:
            logger.info("Trying NETCONF Connection to device {}", self.host)
            self.dev.open()
            self.conn_status = True
            logger.success("NETCONF Connection to device {} succesful", self.host)
            
        except ConnectError as err:
            logger.error("Cannot connect to device {}. Error: {}", self.host, err)
            self.conn_status = False

        return self.conn_status


    @logger.catch
    def disconnect(self):
        """[Disconnect session to device]
        """
        self.dev.close()
        logger.info("Disconnected from device {}", self.host)
        self.conn_status = False


    @logger.catch
    def lock_config(self, edit_mode='private', MAX_RETRIES=5):
        """[Enter config_lock mode on device]

        Args:
            MAX_RETRIES (int, optional): [Maximum number of attempts to retry config_lock if failed]. Defaults to 5.

        Returns:
            [Boolean]: [Config Lock status]
        """

        if not self.conn_status:
            self.connect()

        try:
            #self.dev.bind(cu=Config, mode='private')
            if edit_mode:
                self.cu = Config(self.dev, mode=edit_mode)
            else:
                self.cu = Config(self.dev)
        except ConnectError as conn_err:
            logger.error("Connect Error {}", conn_err)
        except Exception as err:
            print(err)
            logger.error("Exception {}", err)


        if not self.config_locked:
            while not self.config_locked and MAX_RETRIES > 0:
                try:
                    logger.info("Trying config lock on device {}", self.host)
                    self.cu.lock()
                    self.config_locked = True
                    logger.success("Configuration lock on device {} was successful", self.host)
                    break

                except LockError as err:
                    MAX_RETRIES -= 1
                    self.config_locked = False
                    logger.warning("Unable to lock configuration: {}. Remaining tries: {} Error: {}", err, MAX_RETRIES, err)
                    time.sleep(3)

        return self.config_locked


    @logger.catch
    def unlock_config(self, MAX_RETRIES=5):

        """[Exit config_lock mode on device]

        Args:
            MAX_RETRIES (int, optional): [Maximum number of attempts to retry unlock_config if failed]. Defaults to 5.

        Returns:
            [Boolean]: [Config Lock status]
        """

        if self.config_locked:
            while self.config_locked and MAX_RETRIES > 0: 
                try:
                    logger.info("Trying unlock_config on device {}", self.host)
                    self.cu.unlock()
                    self.config_locked = False
                    logger.success("Unlock config on device {} was successful", self.host)
                    break
                except UnlockError as err:
                    MAX_RETRIES -= 1
                    logger.warning("Unable to unlock config on device {} Remaining tries: {} Error: {}", self.host, MAX_RETRIES, err)
                    self.config_locked = True
                    time.sleep(3)

        
        return self.config_locked

    @logger.catch
    def get_interface_info(self, interfaceName=None, stats=False, terse=False, brief=False, extensive=False, data_format='json'):
        """[Get interface operational information from device]

        Args:
            interfaceName ([string], optional): [interface Name. Example: interfaceName="ge-0/0/1"]. Defaults to None.
            stats (bool, optional): [True if operational statistics required]. Defaults to False.
            terse (bool, optional): [True to return only terse level information]. Defaults to False.
            brief (bool, optional): [True to return only brief level information]. Defaults to False.
            extensive (bool, optional): [True to return detailed/extensive level information]. Defaults to False.
            data_format (str, optional): [Data format to return information]. Defaults to 'json'. Valid data_formats: ['text', 'xml', 'json']

        Returns:
            [Tuple]: [result_state: True if information collected, result_data: Interface information]
        """
        if interfaceName:
            try:
                logger.info("Getting interface information for device {} for interface {}", self.host, interfaceName)
                if brief: 
                    result = self.dev.rpc.get_interface_information({'format':data_format}, interface_name=interfaceName, statistics=stats, brief=True)
                elif terse:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, interface_name=interfaceName, statistics=stats, terse=True)
                elif extensive:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, interface_name=interfaceName, statistics=stats, extensive=True)
                else:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, interface_name=interfaceName, statistics=stats)
                result_state = True

            except Exception as err:
                logger.error("Exception encountered for device {} Error: {}}", self.host, err)
                result = (f"An exception was encountered {err}")
                result_state = False

        else:
            try:
                logger.info("Getting interface information for device {} for all interfaces", self.host)
                if brief:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, statistics=stats, brief=True)
                elif terse:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, statistics=stats, terse=True)
                elif extensive:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, statistics=stats, detail=True)
                else:
                    result = self.dev.rpc.get_interface_information({'format':data_format}, statistics=stats)
                result_state = True

            except Exception as err:
                logger.error("Exception encountered for device {} Error: {}", self.host, err)
                result = (f"An exception was encountered {err}")
                result_state = False

        logger.debug("Interface_info {}", result)
        return result_state, result


    @logger.catch
    def _check_data_format(self, data_format):
        if data_format not in ['text', 'xml', 'json', 'set']:
            logger.error("Invalid data format: {}", data_format)
            raise ValueError("Invalid Data format")
        
        else:
            return True

    @logger.catch
    def get_configuration(self, interfaceName=None, data_format='text'):
        """
        [Get configuration from device]

        Args:
            interfaceName ([string], optional): [interfaceName for which to filter configuration information]. Defaults to None
            data_format (string, optional): [Data format of configuration]. Valid data_formats: ['text', 'xml', 'json', 'set']

        Returns:
            [Tuple]: [(result: status of get configuration [True if success, False if Failed], Details if any)]
        """

        self._check_data_format(data_format)

        if interfaceName:
            config_filter = "<interfaces><interface><name>%s</name></interface></interfaces>"""%interfaceName

            try:
                result = self.dev.rpc.get_config(options={'format':data_format}, filter_xml=config_filter)
                result_state = True
            except Exception as err:
                logger.error("Exception encountered when retrieving configuration for device {} Error: {}", self.host, err)
                result = f"Exception encountered when retrieving configuration for device {self.host} \nError: {err}"
                result_state = False
        else:
            try:
                result = self.dev.rpc.get_config(options={'format':data_format})
                result_state = True
            except Exception as err:
                logger.error("Exception encountered when retrieving configuration for device {} Error: {}", self.host, err)
                result = f"Exception encountered when retrieving configuration for device {self.host} \nError: {err}"
                result_state = False

        if result_state:
            if data_format == 'text':
                result_data = result.text
            elif data_format == 'xml':
                result_data = etree.tounicode(result)
            else:
                result_data = result
        else:
            result_data = result

        return result_state, result_data

    @logger.catch
    def load_configuration(self, config, config_mode='merge', data_format='xml'):
        """
        [Load Configuration on device]

        Args:
            config ([string]): [Configuration to be loaded to device].
            config_mode (string, optional): [Configuration Load mode]. Defaults to 'merge'. Valid options ['merge', 'override']
            data_format (string, optional): [Data format of configuration]. Valid data_formats: ['text', 'xml', 'json', 'set']

        Returns:
            [Tuple]: [(result: status of load configuration [True if success, False if Failed], Details if any)]
        """
        
        self._check_data_format(data_format)

        result_data = False
        MAX_COMMITCHECK_RETRY = 5
        diff = None
        if not self.config_locked:
            self.lock_config()
        COMMIT_CHECK_RESULT = False
        try:
            if config_mode == 'merge':
                self.cu.load(config, format=data_format, merge=True)
            elif config_mode == 'override':                    
                self.cu.load(config, format=data_format, overwrite=True)

            diff = self.cu.diff()
            logger.debug("Configuration loaded to device {}. Diff: \n {}", self.host, diff)

            # Doing COMMIT_CHECK for syntax validation of config changes. If Failed, cannot commit config changes.
            while MAX_COMMITCHECK_RETRY > 0:
                try:
                    logger.info("Attempting commit check on device {}", self.host)
                    COMMIT_CHECK_RESULT = self.cu.commit_check()

                except Exception as err:
                    logger.error("Exception encountered for commit check on device {}. Config diff: \n{}", self.host, diff)
                    print("Exception encountered for stage1 commit check\n{}".format(err))
                    COMMIT_CHECK_RESULT = False
                    result_data = err

                if COMMIT_CHECK_RESULT:
                    break
                else:
                    MAX_COMMITCHECK_RETRY -= 1
                    time.sleep(3)

        except ConfigLoadError as err:
            logger.warning("ConfiguLoadError: Unable to load configuration to device {}. Error {}\nUnlocking config", self.host, err)
            self.unlock_config()
            result = False
        
        except Exception as err:
            logger.error("Exception encountered while loading configuration on device {} Error: {}", self.host, err)
            result = False

        if COMMIT_CHECK_RESULT:
            logger.success("Commit check on device {} was successful", self.host)
            result = True
        else:
            logger.warning("All attempts for commit check on device {} failed. Diff\n {}", self.host, diff)
            result = False

            #self.unlock_config()
        """
        else:
            result = False
        """
        if result:
            result_data = diff
        else:
            if not result_data:
                result_data = f"Unable to get into configuration lock mode on device {self.host}"
            

        return result, result_data

    @logger.catch
    def commit_config(self, comment=None, confirmed=False, rollback_timer=5):
        """[Commit Configuration on device]

        Args:
            comment ([string], optional): [Comment asssociated with commit operation]. Defaults to None.
            confirmed (bool, optional): [Confirmed enables, auto-rollback of the commit after rollback_timer]. Defaults to False.
            rollback_timer (int, optional): [Time in minutes for which commit will rollback if Commit Confirmed]. Defaults to 5.

        Returns:
            [Tuple]: [(COMMIT_STATUS, COMMIT_INFO)]
        """
        if comment:
            comment_note = comment
        else:
            comment_note = "Commit by orch platform"

        if self.config_locked:
            COMMIT_STATUS = False
            try:
                if confirmed:
                    COMMIT_STATUS = self.cu.commit(confirm=rollback_timer, comment=comment_note)
                else:
                    COMMIT_STATUS = self.cu.commit(comment=comment_note)
                
            except Exception as err:
                COMMIT_STATUS = False
                COMMIT_DATA = f"Unable to commit configuration on device {self.host}. Error: {err}"
                logger.error("Unable to commit configuration on device {}. Error: {}", self.host, err)

        else:
            MSG = f"Unable to commit configuration on device {self.host}. Not in config mode"
            logger.error("REMOVE: Unable to commit configuration on device {}. Not in config mode", self.host)
            logger.error(MSG)
            COMMIT_STATUS = False
            COMMIT_DATA = MSG

        if COMMIT_STATUS:
            COMMIT_DATA = f"Successful commit on device {self.host}. Auto-rollback in {rollback_timer} min" if confirmed else f"Successful commit"
            logger.success(COMMIT_DATA)
        else:
            logger.error("Commit failed on device {}", self.host)
            COMMIT_DATA = err
        
        return COMMIT_STATUS, COMMIT_DATA
        
    @logger.catch
    def rollback_config(self, rollback_id):
        """[Load rollback configuration on device]

        Args:
            rollback_id ([int]): [Rollback id for which to load configuration]. Defaults to int.

        Returns:
            [Tuple]: [ROLLBACK_LOAD status if loading rollback config was successful, ROLLBACK_DIFF: diff of rollback configuration]
        """
        ROLLBACK_LOAD = False
        ROLLBACK_DIFF = None
        if not self.config_locked:
            self.lock_config()
            try:
                self.cu.rollback(rb_id=rollback_id)
                ROLLBACK_LOAD = True
                ROLLBACK_DIFF = self.cu.diff()
                logger.success("Rollback configuration loaded successfully on device {}. Diff:\n{}", self.host, ROLLBACK_DIFF)

            except Exception as err:
                logger.error("Unable to rollback configuration on device {}")
                ROLLBACK_LOAD = False
                ROLLBACK_DIFF = None

        return ROLLBACK_LOAD, ROLLBACK_DIFF
