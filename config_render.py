#!/usr/bin/env python3

from jinja2 import Template
from loguru import logger
import yaml

logger.add('log/config_gen.log', format="{time} {level} | {module}:{function}| {message}", level="DEBUG")


class ConfigGen(object):
    def __init__(self, deviceName):
        self.device = deviceName
        with open('inventory/inventory.yml', 'r') as iv:
            self.inventory_list = yaml.safe_load(iv.read())['devices']

    def config_render(self, config_data):
        """[Render device configuration from variable data]

        Args:
            config_data ([dict]): [Config data used as variables to generate configuration from template]

        Raises:
            ValueError: [Raised when device not found in inventory]

        Returns:
            [tuple]: [(Boolean for state of config generation, Generated configuration)]
        """
        if self.device in self.inventory_list:
            try:
                template_name = "templates/" + self.inventory_list[self.device].get('vendor') + "_interface.j2"
                with open(template_name, 'r') as t:
                    template = Template(t.read())
                    config = template.render(config_data)
                    result_state = True

            except Exception as err:
                logger.error("Exception encountered: {}", err)
                config = None
                result_state = False
        else:
            logger.error("Device {} does not exist in inventory", self.device)
            config = None
            result_state = False
            raise ValueError(f"Device {self.device} does not exist in inventory")

        return result_state, config